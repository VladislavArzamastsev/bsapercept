package bsa.java.concurrency.hash;

import java.util.concurrent.CompletableFuture;

public interface Hasher {

    CompletableFuture<Long> hash(byte[] image);

    Long hashNow(byte[] image);
}
