package bsa.java.concurrency.dao;

import bsa.java.concurrency.dto.SearchResultDTO;
import bsa.java.concurrency.entity.Image;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface DatabaseImageDao extends CrudRepository<Image, UUID> {

    @Async
    @Query(value = "INSERT INTO images(id, hash, url) VALUES (:#{#image.id}, :#{#image.hash}, :#{#image.url})",
            nativeQuery = true)
    @Transactional
    @Modifying
    void asynchronousSave(@Param("image") Image image);

    @Query(value =
            "SELECT id AS imageId, url AS imageUrl, " +
            "1 - CAST(LENGTH(replace(CAST(CAST(images.hash # :hash AS bit(64)) AS TEXT), '0', '')) AS NUMERIC) / 64 AS matchPercent " +
            "FROM images " +
            "WHERE 1 - CAST(LENGTH(replace(CAST(CAST(images.hash # :hash AS bit(64)) AS TEXT), '0', '')) AS NUMERIC) / 64 >= :threshold",
            nativeQuery = true)
    List<SearchResultDTO> searchMatches(long hash, double threshold);

    @Override
    @Async
    void deleteById(UUID imageId);

    @Override
    @Async
    void deleteAll();
}
