package bsa.java.concurrency.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

@Repository
public class FileSystemImageDaoImpl implements FileSystemImageDao {

    private static final Path ROOT_PATH = Paths.get(".").toAbsolutePath().resolve("files");
    private static final String JPG_EXTENSION = ".jpg";
    private final ExecutorService executorService;

    static {
        setupStorage();
    }

    @Autowired
    public FileSystemImageDaoImpl(ExecutorService executorService) {
        this.executorService = executorService;
    }

    @Override
    public CompletableFuture<Path> save(MultipartFile image, UUID imageId) {
        Path destination = getPathForImage(imageId);
        return CompletableFuture.supplyAsync(() -> {
            try {
                return Files.write(destination,
                        image.getBytes(),
                        StandardOpenOption.WRITE,
                        StandardOpenOption.CREATE,
                        StandardOpenOption.TRUNCATE_EXISTING);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Override
    public CompletableFuture<Void> deleteById(UUID imageId) {
        return CompletableFuture.runAsync(() -> {
            try {
                Files.deleteIfExists(getPathForImage(imageId));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }, executorService);
    }

    @Override
    public CompletableFuture<Void> deleteAll() {
        return CompletableFuture.runAsync(() -> {
            try {
                File[] files = ROOT_PATH.toFile().listFiles();
                if (files != null) {
                    for (File file : files) {
                        Files.delete(file.toPath());
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }, executorService);
    }

    @Override
    public CompletableFuture<byte[]> getSavedByFilename(String filename) {
        Path path = getPathForImage(filename);
        return CompletableFuture.supplyAsync(() -> {
            try {
                return Files.readAllBytes(path);
            } catch (NoSuchFileException e){
                return new byte[]{};
            }catch (IOException e) {
                throw new RuntimeException(e);
            }
        }, executorService);
    }

    private static Path getPathForImage(String filename) {
        return ROOT_PATH.resolve(filename);
    }

    private static Path getPathForImage(UUID imageId) {
        return ROOT_PATH.resolve(imageId.toString().concat(JPG_EXTENSION));
    }

    private static void setupStorage() {
        try {
            if (!Files.exists(ROOT_PATH)) {
                Files.createDirectory(ROOT_PATH);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
