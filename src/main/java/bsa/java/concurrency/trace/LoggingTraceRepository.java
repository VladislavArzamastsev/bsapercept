package bsa.java.concurrency.trace;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.trace.http.HttpTrace;
import org.springframework.boot.actuate.trace.http.HttpTraceRepository;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Component
public final class LoggingTraceRepository implements HttpTraceRepository {

    private static final Logger HTTP_LOGGER = LoggerFactory.getLogger("http-logger");

    // For security matters it's better to not expose Traces on HTTP
    @Override
    public List<HttpTrace> findAll() {
        return new ArrayList<>();
    }

    @Override
    public void add(HttpTrace trace) {
        HTTP_LOGGER.info(getTraceInfo(trace));
    }

    private String getTraceInfo(HttpTrace trace) {
        String requestInfo = getRequestInfo(trace.getRequest());
        String responseInfo = getResponseInfo(trace.getResponse());
        return String.format("%s%s", requestInfo, responseInfo);
    }

    private String getRequestInfo(HttpTrace.Request request) {
        URI uri = request.getUri();
        String method = request.getMethod();
        String remoteAddress = request.getRemoteAddress();
        String headers = request.getHeaders().toString();
        return String.format("%nRequest:%n\tURI: %s%n\tMethod: %s%n\tRemoteAddress: %s%n\tHeaders: %s",
                uri.toString(), method, remoteAddress, headers);
    }

    private String getResponseInfo(HttpTrace.Response response) {
        int status = response.getStatus();
        String headers = response.getHeaders().toString();
        return String.format("%nResponse:%n\tStatus: %d%n\tHeaders: %s",
                status, headers);
    }
}
