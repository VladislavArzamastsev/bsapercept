package bsa.java.concurrency.dto;

import java.util.UUID;

public interface SearchResultDTO {
    UUID getImageId();
    Double getMatchPercent();
    String getImageUrl();
}
