package bsa.java.concurrency.controller;

import bsa.java.concurrency.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/files")
public final class SavedImageController {

    private final ImageService imageService;

    @Autowired
    public SavedImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @GetMapping(
            value = "/{filename}",
            produces = MediaType.IMAGE_JPEG_VALUE
    )
    public ResponseEntity<byte[]> getImageWithMediaType(@PathVariable("filename") String filename){
        byte[] savedByFilename = imageService.getSavedByFilename(filename);
        HttpStatus status = savedByFilename.length != 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(savedByFilename, status);
    }

}
