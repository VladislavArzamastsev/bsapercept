package bsa.java.concurrency.controller;

import bsa.java.concurrency.dto.SearchResultDTO;
import bsa.java.concurrency.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/image")
public final class ImageController {

    private final ImageService imageService;

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @PostMapping("/batch")
    public ResponseEntity<Void> batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        CompletableFuture<Void> voidCompletableFuture = imageService.batchUploadImages(files);
        try {
            voidCompletableFuture.get();
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(
            @RequestParam("image") MultipartFile file,
            @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) {
        List<SearchResultDTO> searchResultDTOS = imageService.searchMatches(file, threshold);
        if(searchResultDTOS.isEmpty()){
            imageService.batchUploadImages(new MultipartFile[]{file});
        }
        return searchResultDTOS;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) {
        imageService.deleteImage(imageId);
    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages(){
        imageService.purgeImages();
    }
}
