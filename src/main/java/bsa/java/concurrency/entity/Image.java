package bsa.java.concurrency.entity;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "images")
@Data
public class Image {

    @Id
    @Type(type="uuid-char")
    private UUID id;

    @Column(name = "hash")
    private Long hash;

    @Column(name = "url")
    String url;
}
