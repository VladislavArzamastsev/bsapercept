package bsa.java.concurrency.service;

import bsa.java.concurrency.dto.SearchResultDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public interface ImageService {

    CompletableFuture<Void> batchUploadImages(MultipartFile[] files);

    byte[] getSavedByFilename(String id);

    List<SearchResultDTO> searchMatches(MultipartFile file, double threshold);

    void deleteImage(UUID imageId);

    void purgeImages();
}
