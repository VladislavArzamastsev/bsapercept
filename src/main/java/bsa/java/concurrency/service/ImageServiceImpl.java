package bsa.java.concurrency.service;

import bsa.java.concurrency.dao.DatabaseImageDao;
import bsa.java.concurrency.dao.FileSystemImageDao;
import bsa.java.concurrency.dto.SearchResultDTO;
import bsa.java.concurrency.entity.Image;
import bsa.java.concurrency.hash.Hasher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

@Service
public final class ImageServiceImpl implements ImageService {

    private final ExecutorService executorService;
    private final Hasher hasher;
    private final FileSystemImageDao fileSystemImageDao;
    private final DatabaseImageDao databaseImageDao;
    private final ImageUrlConstructor imageUrlConstructor;

    @Autowired
    public ImageServiceImpl(ExecutorService executorService, Hasher hasher,
                            FileSystemImageDao fileSystemImageDao,
                            DatabaseImageDao databaseImageDao,
                            ImageUrlConstructor imageUrlConstructor) {
        this.executorService = executorService;
        this.hasher = hasher;
        this.fileSystemImageDao = fileSystemImageDao;
        this.databaseImageDao = databaseImageDao;
        this.imageUrlConstructor = imageUrlConstructor;
    }

    @Override
    public CompletableFuture<Void> batchUploadImages(MultipartFile[] files) {
        return CompletableFuture.runAsync(() -> {
            for (MultipartFile file : files) {
                if (!file.isEmpty()) {
                    try {
                        CompletableFuture<Long> hashFuture = hasher.hash(file.getBytes());
                        UUID imageId = UUID.randomUUID();
                        CompletableFuture<Path> save = fileSystemImageDao.save(file, imageId);
                        save.thenAccept(path -> saveToDatabase(imageId, path.getFileName(), hashFuture));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, executorService);
    }

    private void saveToDatabase(UUID imageId, Path fileName, Future<Long> hashFuture) {
        try {
            Image image = new Image();
            image.setId(imageId);
            image.setHash(hashFuture.get());
            image.setUrl(imageUrlConstructor.constructImageUrl(fileName));
            databaseImageDao.asynchronousSave(image);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    @Override
    public byte[] getSavedByFilename(String filename) {
        try {
            return fileSystemImageDao.getSavedByFilename(filename).get();
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<SearchResultDTO> searchMatches(MultipartFile file, double threshold) {
        try {
            Long hash = hasher.hashNow(file.getBytes());
            return databaseImageDao.searchMatches(hash, threshold);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteImage(UUID imageId) {
        databaseImageDao.deleteById(imageId);
        fileSystemImageDao.deleteById(imageId);
    }

    @Override
    public void purgeImages() {
        databaseImageDao.deleteAll();
        fileSystemImageDao.deleteAll();
    }
}
