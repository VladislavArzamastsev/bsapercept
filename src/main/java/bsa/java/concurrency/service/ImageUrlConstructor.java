package bsa.java.concurrency.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;

@Component
final class ImageUrlConstructor {

    @Value("${server.address}")
    private String serverAddress;
    @Value("${server.port}")
    private int serverPort;

    String constructImageUrl(Path fileName){
        try {
            URL url = new URL("http", serverAddress, serverPort,
                    "/files/".concat(fileName.toString()));
            return url.toString();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

}
